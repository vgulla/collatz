#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List, Dict

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i                                                                                                                                                             , j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

#Lazy Cache based on class discussions
lazycache = dict()

def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    if i > j:
        i += j
        j = i - j
        i -= j
    if j >= 1000000 or i < 0:
        return 0
    if i < j >> 1:
        i = j >> 1
    mcl = 0
    x = i
    while x <= j:
        if x in lazycache:
            length = lazycache[x]
        else:
            length = cycle_length(x)
            lazycache[x] = length
        if length > mcl:
            mcl = length
        x += 1
    assert mcl > 0
    assert lazycache != {}
    return mcl

# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)

def cycle_length(i: int) -> int:
    """
    i the number to find the cycle length of
    """
    assert i >= 0
    if i == 0:
        return 0
    if i == 1:
        return 1
    length = 1
    while i > 1:
        if i in lazycache:
            return lazycache[i] + length - 1
        if i % 2 == 0:
            i = i // 2
            length += 1
        else: 
            # Optimization based on in class quiz and discussion
            i = (3 * i) // 2 + 1
            length += 2
    return length
